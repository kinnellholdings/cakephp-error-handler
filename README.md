# Cake PHP Error Handler

## Exceptions

Stops CakePHP from logging 404 errors with a 10 line stack trace.

**Usage:**

Install via `composer` and in `app/Config/core.php` replace:

```
Configure::write('Exception', array(
    'handler' => 'ErrorHandler::handleException',
    'renderer' => 'ExceptionRenderer',
    'log' => true
));
```

With:

```
Configure::write('Exception', array(
    'handler' => '\Kinnell\KinnellErrorHandler::handleException',
    'renderer' => 'ExceptionRenderer',
    'log' => true
));
```

## Errors

Replace:

```
Configure::write('Error', array(
    'handler' => 'ErrorHandler::handleError',
    'level' => E_ALL & ~E_STRICT,
    'trace' => true
));
```

With (non-standard):

```
Configure::write('Error', [
  'handler'  => '\Kinnell\KinnellErrorHandler::handleError',
  'level'    => E_ALL,
  'log'      => true, // Send error/notice to syslog via CakeLog
  'trace'    => true, // Send error/notice trace to syslog via CakeLog
  'send_err' => true, // Send error/trace as part of the response
]);
```
