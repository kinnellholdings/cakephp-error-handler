<?php
namespace Kinnell;

\App::uses('ErrorHandler', 'Error');

class KinnellErrorHandler extends \ErrorHandler
{
  public static function handleException($exception)
  {
    $config = \Configure::read('Exception');
    $skip = [
      'MissingControllerException',
      'MissingActionException',
      'PrivateActionException',
      'NotFoundException'
    ];
    //Don't log 404 errors
    if (!in_array(get_class($exception), $skip)) {
      self::_log($exception, $config);
    }

    $renderer = isset($config['renderer']) ? $config['renderer'] : 'ExceptionRenderer';
    if ($renderer !== '\ExceptionRenderer') {
      list($plugin, $renderer) = pluginSplit($renderer, true);
      \App::uses($renderer, $plugin . 'Error');
    }
  
    try {
  
      $error = new $renderer($exception);
      $error->render();
  
    } catch (\Exception $e) {
  
      set_error_handler(\Configure::read('Error.handler')); // Should be using configured ErrorHandler
      \Configure::write('Error.trace', false); // trace is useless here since it's internal
      $message = sprintf("[%s] %s\n%s", // Keeping same message format
        get_class($e),
        $e->getMessage(),
        $e->getTraceAsString()
      );

      self::$_bailExceptionRendering = true;
      trigger_error($message, E_USER_ERROR);

    }
  }

  public static function handleError($code, $description, $file = null, $line = null, $context = null)
  {
    if (error_reporting() === 0) {
      return false;
    }

    list($error, $log) = static::mapErrorCode($code);

    if ($log == LOG_ERR) {
      return static::handleFatalError($code, $description, $file, $line);
    }

    $error_config = \Configure::read('Error');

    $message = vsprintf('%s (%s): %s in [\'%s\', line %s]', [
      $error,
      $code,
      $description,
      $file,
      $line,
    ]);

    if (isset($error_config['log']) && $error_config['log']) {
      \CakeLog::write($log, $message);
    }

    if (!empty($error_config['trace'])) {
      if (version_compare(PHP_VERSION, '5.4.21', '<')) {
        if (!class_exists('Debugger')) {
          App::load('Debugger');
        }
        if (!class_exists('CakeText')) {
          App::uses('CakeText', 'Utility');
          App::load('CakeText');
        }
      }
    }

    if (isset($error_config['trace']) && $error_config['trace']) {
      $trace = \Debugger::trace(['start' => 1, 'format' => 'log']);
      \CakeLog::write($log, $trace);
    }

    $debug = \Configure::read('debug');
    if ($debug && isset($error_config['send_err']) && $error_config['send_err']) {
      $data = [
        'level'       => $log,
        'code'        => $code,
        'error'       => $error,
        'description' => $description,
        'file'        => $file,
        'line'        => $line,
        'context'     => $context,
        'start'       => 2,
        'path'        => \Debugger::trimPath($file),
      ];
      return \Debugger::getInstance()->outputError($data);
    }
  }
}
